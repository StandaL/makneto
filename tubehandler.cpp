/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "tubehandler.h"
#include "contactsview.h"
#include "chatwindow.h"

#include <QDebug>
#include <QDBusConnection>

#include <TelepathyQt/Types>
#include <TelepathyQt/Account>
#include <TelepathyQt/PendingDBusTubeConnection>
#include <TelepathyQt/AbstractClient>
#include <TelepathyQt/ChannelClassSpec>
#include <TelepathyQt/IncomingDBusTubeChannel>
#include <TelepathyQt/OutgoingDBusTubeChannel>

Q_DECLARE_METATYPE(Tp::AccountPtr)
Q_DECLARE_METATYPE(Tp::ContactPtr)

TubeHandler::TubeHandler(ContactsView *parent) :
    QObject(parent),
    Tp::AbstractClientHandler(Tp::ChannelClassSpecList() << Tp::ChannelClassSpec::incomingDBusTube("org.qt.standal.makneto")
                               << Tp::ChannelClassSpec::outgoingDBusTube("org.qt.standal.makneto")
                               << Tp::ChannelClassSpec::incomingRoomDBusTube("org.qt.standal.makneto")
                               << Tp::ChannelClassSpec::outgoingRoomDBusTube("org.qt.standal.makneto"),
                                Capabilities(), true),
    mParentWindow(parent)
{

}

TubeHandler::~TubeHandler() {
}

bool TubeHandler::bypassApproval() const {
    return true;
}

void TubeHandler::handleChannels(const Tp::MethodInvocationContextPtr<> &context, const Tp::AccountPtr &account,
                                 const Tp::ConnectionPtr &connection, const QList<Tp::ChannelPtr> &channels,
                                 const QList<Tp::ChannelRequestPtr> &requestsSatisfied,
                                 const QDateTime &userActionTime, const HandlerInfo &handlerInfo)
{
    Q_UNUSED(connection)
    Q_UNUSED(requestsSatisfied)
    Q_UNUSED(userActionTime)
    Q_UNUSED(handlerInfo)

    foreach(const Tp::ChannelPtr &channel, channels) {
            QVariantMap properties = channel->immutableProperties();

            //Q_ASSERT (properties.value(TP_QT_IFACE_CHANNEL + QLatin1String(".ChannelType")) == TP_QT_IFACE_CHANNEL_TYPE_DBUS_TUBE);
            if (properties.value(TP_QT_IFACE_CHANNEL + QLatin1String(".ChannelType")) != TP_QT_IFACE_CHANNEL_TYPE_DBUS_TUBE) {
                qWarning() << "Cannot handle channels of type" << properties.value(TP_QT_IFACE_CHANNEL + QLatin1String(".ChannelType"));
                continue;
            }

            qDebug() << "It's a DBUS Tube:" << properties.value(TP_QT_IFACE_CHANNEL + QLatin1String(".ChannelType"));
            context->setFinished();

            if (properties.value(TP_QT_IFACE_CHANNEL + QLatin1String(".Requested")).toBool()) {
                qDebug() << "Outgoing.....!!!!!";
                Tp::OutgoingDBusTubeChannelPtr outgoingGroupDBusChannel = Tp::OutgoingDBusTubeChannelPtr::qObjectCast(channel);
                Tp::PendingDBusTubeConnection *pendingConnection = outgoingGroupDBusChannel->offerTube(QVariantMap());

                QString accUID(account->uniqueIdentifier()), contactId(outgoingGroupDBusChannel->targetContact()->id());
                if(mParentWindow->getWindow(accUID, contactId) == 0) {
                    ChatWindow *window = new ChatWindow(mParentWindow, account, outgoingGroupDBusChannel->targetContact());
                    window->setAttribute(Qt::WA_DeleteOnClose, true);
                    connect(window, SIGNAL(myDestroyed(QString,QString)), mParentWindow, SLOT(removeOpened(QString,QString)));
                    window->show();
                    mParentWindow->addOpened(accUID, contactId, window);
                }

                pendingConnection->setProperty("account", QVariant::fromValue(account));
                pendingConnection->setProperty("contact", QVariant::fromValue(outgoingGroupDBusChannel->targetContact()));
                connect(pendingConnection,
                        SIGNAL(finished(Tp::PendingOperation*)),
                        SLOT(onTubeReady(Tp::PendingOperation*)));
            } else {
                qDebug() << "Incoming.....!!!!!" << channel->initiatorContact()->id();
                Tp::IncomingDBusTubeChannelPtr incomingGroupDBusChannel = Tp::IncomingDBusTubeChannelPtr::qObjectCast(channel);
                Tp::PendingDBusTubeConnection *pendingConnection = incomingGroupDBusChannel->acceptTube();

                QString accUID(account->uniqueIdentifier()), contactId(incomingGroupDBusChannel->initiatorContact()->id());
                if(mParentWindow->getWindow(accUID, contactId) == 0) {
                    ChatWindow *window = new ChatWindow(mParentWindow, account, incomingGroupDBusChannel->initiatorContact());
                    window->setAttribute(Qt::WA_DeleteOnClose, true);
                    connect(window, SIGNAL(myDestroyed(QString,QString)), mParentWindow, SLOT(removeOpened(QString,QString)));
                    window->show();
                    mParentWindow->addOpened(accUID, contactId, window);
                }
                pendingConnection->setProperty("account", QVariant::fromValue(account));
                pendingConnection->setProperty("contact", QVariant::fromValue(incomingGroupDBusChannel->initiatorContact()));
                connect(pendingConnection,
                        SIGNAL(finished(Tp::PendingOperation*)),
                        SLOT(onTubeReady(Tp::PendingOperation*)));
            }
        }
}

void TubeHandler::onTubeReady(Tp::PendingOperation *op) {
    if(op->isError()) {
        qDebug() << op->errorMessage();
        return;
    }
    Tp::PendingDBusTubeConnection *conn = qobject_cast<Tp::PendingDBusTubeConnection *>(op);
    Tp::AccountPtr account = conn->property("account").value<Tp::AccountPtr>();
    Tp::ContactPtr contact = conn->property("contact").value<Tp::ContactPtr>();
    if(conn == 0) {
        qDebug() << "Operation is not Tube operation.";
        return;
    }
    qDebug() << conn->address();
    QDBusConnection dbusConn = QDBusConnection::connectToPeer(conn->address(), conn->address().right(8));
    ChatWindow *win = mParentWindow->getWindow(account->uniqueIdentifier(), contact->id());
    win->setConnection(dbusConn);
    //ChatWindow *w = new ChatWindow(mParentWindow, account, contact, dbusConn);
    //w->show();
    //mParentWindow->setConnection(dbusConn, account);
}
