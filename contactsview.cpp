/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "qmlapplicationviewer.h"
#include "msortfilterproxymodel.h"
#include "contactsview.h"
#include "contactsmodel.h"
#include "iconprovider.h"
#include "chatwindow.h"
#include "tubehandler.h"

#include <QtDeclarative>
#include <QDeclarativeEngine>

#include <TelepathyQt/Types>
#include <TelepathyQt/AccountFactory>
#include <TelepathyQt/ConnectionFactory>
#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/ChannelFactory>
#include <TelepathyQt/AccountManager>
#include <TelepathyQt/ClientRegistrar>
#include <TelepathyQt/DBusTubeChannel>

Q_DECLARE_METATYPE(Tp::ContactPtr)
Q_DECLARE_METATYPE(Tp::AccountPtr)

ContactsView::ContactsView(QmlApplicationViewer *viewer) :
    QWidget(),
    mViewer(viewer)
{
    Tp::registerTypes();

    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                       Tp::Features() << Tp::Account::FeatureCore
                                                                       << Tp::Account::FeatureAvatar
                                                                       << Tp::Account::FeatureCapabilities
                                                                       << Tp::Account::FeatureProtocolInfo
                                                                       << Tp::Account::FeatureProfile);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
                                                                               Tp::Features() << Tp::Connection::FeatureCore
                                                                               << Tp::Connection::FeatureRosterGroups
                                                                               << Tp::Connection::FeatureRoster
                                                                               << Tp::Connection::FeatureSelfContact);

    Tp::ContactFactoryPtr contactFactory = Tp::ContactFactory::create(Tp::Features()  << Tp::Contact::FeatureAlias
                                                                      << Tp::Contact::FeatureAvatarData
                                                                      << Tp::Contact::FeatureSimplePresence
                                                                      << Tp::Contact::FeatureCapabilities
                                                                      << Tp::Contact::FeatureClientTypes);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());
    channelFactory->addCommonFeatures(Tp::Channel::FeatureCore);
    channelFactory->addFeaturesForIncomingDBusTubes(Tp::DBusTubeChannel::FeatureCore);
    channelFactory->addFeaturesForOutgoingDBusTubes(Tp::DBusTubeChannel::FeatureCore);

    mRegistrar = Tp::ClientRegistrar::create(accountFactory,
                                             connectionFactory,
                                             channelFactory,
                                             contactFactory);

    Tp::AccountManagerPtr accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
                                                  accountFactory,
                                                  connectionFactory,
                                                  channelFactory,
                                                  contactFactory);

    IconProvider *iconProvider = new IconProvider(QDeclarativeImageProvider::Pixmap);

    openedWindows = new QHash<QString, WindowId>();

    mViewer->engine()->addImageProvider("QIcons", iconProvider);
    mViewer->addImportPath(QLatin1String("modules"));
    mViewer->setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

    mModel = new ContactsModel();
    mModel->setAccountManager(accountManager.data());
    //mViewer->engine()->rootContext()->setContextProperty("_accountManager", accountManager.data());
    mViewer->engine()->rootContext()->setContextProperty("_baseModel", mModel);
    mViewer->setMainQmlFile(QLatin1String("qml/contactlist/main.qml"));

    QObject *rootObject = mViewer->rootObject();
    mListView = rootObject->findChild<QObject *>("contactListView");
    QObject *model = mListView->property("model").value<QObject *>();
    mProxyModel = qobject_cast<MSortFilterProxyModel *>(model);

}

ContactsView::~ContactsView() {
}

int ContactsView::setup() {
    mViewer->showExpanded();
    TubeHandler * handler = new TubeHandler(this);

    if (!mRegistrar->registerClient(Tp::AbstractClientPtr(handler), "Makneto")) {
        qDebug() << "Makneto already running. Exiting";
        return 1;
    }

    QObject::connect(mListView, SIGNAL(mIndexChanged(int)), SLOT(startChat(int)));

    mProxyModel->setSortRole(ContactsModel::StatusRole);
    mProxyModel->setDynamicSortFilter(true);
    mProxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    mProxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    mProxyModel->sort(0);
    QObject::connect(mModel, SIGNAL(toProxySort()), mProxyModel, SLOT(sortOnChanged()));
    //QObject::connect(mProxyModel, SIGNAL(dataChanged(QModelIndex, QModelIndex)), mProxyModel, SLOT(updateProxyModel(QModelIndex, QModelIndex)));

    return 0;
}

void ContactsView::startChat(int row) {
    mProxyModel->index(row, 0, QModelIndex());
    qDebug() << mProxyModel->data(mProxyModel->index(row, 0, QModelIndex()), ContactsModel::ContactRole);
    qDebug() << row;
    Tp::ContactPtr contact = mProxyModel->data(mProxyModel->index(row, 0, QModelIndex()), ContactsModel::ContactRole).value<Tp::ContactPtr>();
    Tp::AccountPtr account = mProxyModel->data(mProxyModel->index(row, 0, QModelIndex()), ContactsModel::AccountRole).value<Tp::AccountPtr>();
    account->createDBusTube(contact, "org.qt.standal.makneto", QDateTime::currentDateTime(), "org.freedesktop.Telepathy.Client.Makneto");
}

void ContactsView::addOpened(const QString &account, const QString &contact, ChatWindow *window) {
    WindowId mWindowId = {contact, window};
    openedWindows->insertMulti(account, mWindowId);
}


void ContactsView::removeOpened(const QString &account, const QString &contact) {
    qDebug() << "account: " << account;
    OpenedWindows::iterator it = openedWindows->find(account);
    if(it != openedWindows->end()) {
        while((it != openedWindows->end()) && (it.key() == account)) {
            if(it.value().contactId == contact) {
                openedWindows->erase(it);
                return;
            }
            it++;
        }
    }
    return;
}


ChatWindow *ContactsView::getWindow(const QString &account, const QString &contact) {
    QList<WindowId> list = openedWindows->values(account);
    if(list.size() > 0) {
        for(int i = 0; i < list.size(); i++) {
            if(list[i].contactId == contact)
                return list[i].win;
        }
        return 0;
    }
    return 0;
}
