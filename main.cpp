/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <QApplication>
#include <QtDeclarative>
#include <QDeclarativeEngine>

#include <TelepathyQt/Types>
#include <TelepathyQt/AccountFactory>
#include <TelepathyQt/ConnectionFactory>
#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/ChannelFactory>
#include <TelepathyQt/AccountManager>
#include <TelepathyQt/ClientRegistrar>
#include <TelepathyQt/DBusTubeChannel>

#include "qmlapplicationviewer.h"
#include "msortfilterproxymodel.h"
#include "contactsmodel.h"
#include "contactsview.h"
#include "iconprovider.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));
    //qmlRegisterType<ContactsModel>("Makneto", 2, 0, "ContactsModel");
    qmlRegisterType<MSortFilterProxyModel>("Makneto", 2, 0, "MSortFilterProxyModel");
    /*Tp::registerTypes();

    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                       Tp::Features() << Tp::Account::FeatureCore
                                                                       << Tp::Account::FeatureAvatar
                                                                       << Tp::Account::FeatureCapabilities
                                                                       << Tp::Account::FeatureProtocolInfo
                                                                       << Tp::Account::FeatureProfile);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
                                                                               Tp::Features() << Tp::Connection::FeatureCore
                                                                               << Tp::Connection::FeatureRosterGroups
                                                                               << Tp::Connection::FeatureRoster
                                                                               << Tp::Connection::FeatureSelfContact);

    Tp::ContactFactoryPtr contactFactory = Tp::ContactFactory::create(Tp::Features()  << Tp::Contact::FeatureAlias
                                                                      << Tp::Contact::FeatureAvatarData
                                                                      << Tp::Contact::FeatureSimplePresence
                                                                      << Tp::Contact::FeatureCapabilities
                                                                      << Tp::Contact::FeatureClientTypes);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());
    channelFactory->addCommonFeatures(Tp::Channel::FeatureCore);
    channelFactory->addFeaturesForIncomingDBusTubes(Tp::DBusTubeChannel::FeatureCore);
    channelFactory->addFeaturesForOutgoingDBusTubes(Tp::DBusTubeChannel::FeatureCore);

    Tp::ClientRegistrarPtr registrar = Tp::ClientRegistrar::create(accountFactory,
                                                                       connectionFactory,
                                                                       channelFactory,
                                                                       contactFactory);

    Tp::AccountManagerPtr accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
                                                  accountFactory,
                                                  connectionFactory,
                                                  channelFactory,
                                                  contactFactory);

    QScopedPointer<QApplication> app(createApplication(argc, argv));
    qmlRegisterType<ContactsModel>("Makneto", 2, 0, "ContactsModel");

    IconProvider *iconProvider = new IconProvider(QDeclarativeImageProvider::Pixmap);
    QmlApplicationViewer viewer;
    viewer.engine()->addImageProvider("QIcons", iconProvider);
    viewer.addImportPath(QLatin1String("modules"));
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);

    viewer.engine()->rootContext()->setContextProperty("_accountManager", accountManager.data());
    viewer.setMainQmlFile(QLatin1String("qml/contactlist/main.qml"));


    QObject *rootObject = viewer.rootObject();
    QObject *listView = rootObject->findChild<QObject *>("contactListView");
    QObject *model = listView->property("model").value<QObject *>();
    ContactsModel *mModel = qobject_cast<ContactsModel *>(model);
    QObject::connect(listView, SIGNAL(mIndexChanged(QVariant)), mModel, SLOT(printData(QVariant)));
    viewer.showExpanded();
    */
    QmlApplicationViewer viewer;
    ContactsView contactList(&viewer);
    if(contactList.setup() == 1) //Makneto already running
        return EXIT_FAILURE;
    return app->exec();
}
