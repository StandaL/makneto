/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include "contactsview.h"

#include <QMainWindow>
#include <QDBusConnection>

#include <TelepathyQt/Types>

class CanvasWindow;

namespace Ui {
class ChatWindow;
}

class ChatWindow : public QMainWindow
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.qt.standal.makneto")
    
public:
    explicit ChatWindow(ContactsView *parent, const Tp::AccountPtr &, const Tp::ContactPtr &);
    void setConnection(const QDBusConnection &dbusconn);
    QString contactId() const;
    QString accountUID() const;
    ~ChatWindow();

public slots:
    void addMessage(const QString &);
    void testingOutput();

private slots:
    void onSendClicked();
    void toggleWhiteboard();

signals:
    void sendMessage(const QString &);
    void myDestroyed(const QString &, const QString &);
    
private:
    Ui::ChatWindow *ui;
    QDBusConnection mDBusConnection;
    Tp::AccountPtr mAccount;    // the account that uses this chat window
    Tp::ContactPtr mContact;   // the person I am currently talking to
    ContactsView *mParent;
    QDockWidget *canvasView;
    CanvasWindow *canvas;
    bool canvasViewShown;
    QSize mSize;
};

#endif // CHATWINDOW_H
