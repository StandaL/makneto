/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef MAKNETOCANVAS_H
#define MAKNETOCANVAS_H

#include <QDeclarativeView>
#include <QDeclarativeComponent>
#include <QPointer>

class QDeclarativeItem;
class FreeHandTool;

class MaknetoCanvas : public QDeclarativeView
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.qt.standal.makneto")

public:
    enum {
        MOVE,
        LINE,
        FREEHAND,
        RECT,
        TEXT
    };

    MaknetoCanvas();
    virtual void mouseReleaseEvent(QMouseEvent *event);
    QDeclarativeItem *paintingRoot() const;
    FreeHandTool *freeHandTool() const;

public slots:
    void selectMove();
    void selectLine();
    void selectRect();
    void selectText();
    void selectFreeHand();

    void paintElement(const int, const QPoint &, const QPoint &, const int);
    void destroyElement(const int);
    virtual void mousePressEvent(QMouseEvent *event);
    void drawLineOn(const int, const QPointF &, const QPointF &);
    void clearCanvasOf(const int);

    void notifyXChange();
    void notifyYChange();
    void notifyX1Change();
    void notifyY1Change();
    void notifyX2Change();
    void notifyY2Change();
    void notifyTextChange();
    void notifyWidthChange();
    void notifyHeightChange();
    void notifyDrawLine(const QPointF &, const QPointF &);
    void notifyClearCanvas();
    void notifyDestroy();

    void changeObjectProperty(const int, const QString &, const int);
    void changeObjectProperty(const int, const QString &);

signals:
    void paintNew(int, QPoint, QPoint, int);
    void mPropertyChanged(int, QString, int);
    void textChanged(int, QString);
    void lineDrawn(int, QPointF, QPointF);
    void clearCanvas(const int);
    void destroyObject(int);

private:
    QString resourcePath();
    int maxOwnObjectName() const;
    int addToHash();

    int objectsCounter;
    int mSelected;
    QPointer<QDeclarativeComponent> mRootComponent;
    QPointer<QObject> mRootObject;
    FreeHandTool *mFreeHand;
    QPoint *mStartPoint;
    QHash<int, QDeclarativeItem *> *canvasObjects;
};

#endif // MAKNETOCANVAS_H
