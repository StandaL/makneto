/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "freehandtool.h"
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QPixmap>
#include <QTimer>

#include <QDebug>

FreeHandTool::FreeHandTool(QDeclarativeItem *parent) :
    QDeclarativeItem(parent),
    mDrawing(false),
    //mDrawingAllowed(false),
    modified(false),
    mPenWidth(1),
    mPenColor(Qt::black)
{
    setFlag(QGraphicsItem::ItemHasNoContents, false);
    setFlag(QGraphicsItem::ItemClipsToShape, true);
    //qDebug() << acceptedMouseButtons();
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);
    mPoints = new QVector<QPointF>();
    mPoints->reserve(10000);

    foreignPoints = new QVector<QPointF>();
    foreignPoints->reserve(10000);

    //setProperty("focus", true);

    connect(this, SIGNAL(paintLine(QPointF, QPointF)), SLOT(drawLineTo(QPointF, QPointF)));
    connect(this, SIGNAL(clearCanvas()), SLOT(clear()));
}

void FreeHandTool::setPenWidth(const int value) {
    mPenWidth = value;
}

int FreeHandTool::penWidth() const {
    return mPenWidth;
}

void FreeHandTool::setPenColor(const QColor &color) {
    mPenColor = color;
}

QColor FreeHandTool::penColor() const {
    return mPenColor;
}

bool FreeHandTool::isEmpty() const {
    return (mPoints->isEmpty() && foreignPoints->isEmpty());
}
/*
bool FreeHandTool::drawingAllowed() const {
    return mDrawingAllowed;
}

void FreeHandTool::slotFocusChanged() {
    mDrawingAllowed = hasFocus();
    emit drawingAllowedChanged();
}
*/
void FreeHandTool::clear() {
    while(mPoints->size() > 0)
        mPoints->remove(0);
    mPoints->reserve(10000);
    update();
}

void FreeHandTool::foreignClear() {
    while(foreignPoints->size() > 0)
        foreignPoints->remove(0);
    foreignPoints->reserve(10000);
    update();
}

void FreeHandTool::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if(event->button() == Qt::LeftButton) {
        fromPoint = event->pos();
        setFocus(true);
        mDrawing = true;
        return;
    }
    else if(event->button() == Qt::RightButton) {
            emit clearCanvas();
            return;
    }
    QDeclarativeItem::mousePressEvent(event);
}

void FreeHandTool::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if((event->buttons() & Qt::LeftButton) && mDrawing) {
            emit paintLine(fromPoint, event->pos());
            fromPoint = event->pos();
    }
}

void FreeHandTool::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if((event->button() == Qt::LeftButton) && mDrawing) {
        if(fromPoint != event->pos()) {
            emit paintLine(fromPoint, event->pos());
            mDrawing = false;
        }
    }
}
/*
void FreeHandTool::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) {
    mDrawingAllowed = true;
    emit drawingAllowedChanged();
}*/

void FreeHandTool::drawLineTo(const QPointF &fromHere, const QPointF &toPoint) {
    mPoints->append(fromHere);
    mPoints->append(toPoint);
    update();
}

void FreeHandTool::drawLineTo(const qreal fromX, const qreal fromY, const qreal toX, const qreal toY) {
    mPoints->append(QPointF(fromX, fromY));
    mPoints->append(QPointF(toX, toY));
    update();
}

void FreeHandTool::foreignDrawLineTo(const QPointF &fromHere, const QPointF &toPoint) {
    foreignPoints->append(fromHere);
    foreignPoints->append(toPoint);
    update();
}

void FreeHandTool::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) {
    painter->save();
    QPen pen(mPenColor, mPenWidth, Qt::SolidLine, Qt::RoundCap,
                 Qt::RoundJoin);
    painter->setPen(pen);

    if(smooth() == true) {
        painter->setRenderHint(QPainter::Antialiasing, true);
    }

    painter->drawLines(mPoints->data(), mPoints->count());
    painter->drawLines(foreignPoints->data(), foreignPoints->count());

    painter->restore();
}
