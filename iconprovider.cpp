/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "iconprovider.h"
#include <QIcon>

IconProvider::IconProvider(ImageType imageType) :
    QDeclarativeImageProvider(imageType)
{
}

QDeclarativeImageProvider::ImageType IconProvider::imageType() const {
    return Pixmap;
}

QPixmap IconProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) {
    const QPixmap mPixmap = QIcon::fromTheme(id).pixmap(requestedSize);
    *size = mPixmap.size();
    return mPixmap;
}
