/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "chatwindow.h"
#include "ui_chatwindow.h"
#include "contactsview.h"
#include "canvaswindow.h"
#include "freehandtool.h"
#include "maknetocanvas.h"

#include <QDBusConnection>
#include <QDockWidget>

#include <TelepathyQt/Types>
#include <TelepathyQt/AccountFactory>
#include <TelepathyQt/ContactFactory>

const int MAXSIZE = 16777215;

Q_DECLARE_METATYPE(Tp::AccountPtr)
Q_DECLARE_METATYPE(Tp::ContactPtr)

ChatWindow::ChatWindow(ContactsView *parent, const Tp::AccountPtr &account, const Tp::ContactPtr &contact) :
    QMainWindow(parent),
    ui(new Ui::ChatWindow),
    mDBusConnection(QDBusConnection::sessionBus()),
    mAccount(account),
    mContact(contact),
    mParent(parent)
{
    ui->setupUi(this);
    this->setWindowTitle(contact->alias());

    canvas = new CanvasWindow(this);
    canvasView = new QDockWidget(this);
    canvasView->setFloating(false);
    canvasView->setFeatures(QDockWidget::NoDockWidgetFeatures);
    canvasView->setShown(false);
    canvasViewShown = false;
    canvasView->setWidget(canvas);
    addDockWidget(Qt::TopDockWidgetArea, canvasView);

    connect(this, SIGNAL(sendMessage(QString)), SLOT(addMessage(QString)));
    connect(ui->sendBtn, SIGNAL(clicked()), SLOT(onSendClicked()));
    connect(ui->convInput, SIGNAL(returnPressed()), SLOT(onSendClicked()));
    connect(ui->whiteboardButton, SIGNAL(clicked()), SLOT(toggleWhiteboard()));

    mSize = size();

    qDebug() << "Chat" << contact->alias() << "is now connected via" << account->uniqueIdentifier();

}

ChatWindow::~ChatWindow()
{
    emit myDestroyed(mAccount->uniqueIdentifier(), mContact->id());
    delete ui;
}

QString ChatWindow::contactId() const {
    return mContact->id();
}

QString ChatWindow::accountUID() const {
    return mAccount->uniqueIdentifier();
}

void ChatWindow::addMessage(const QString &msg) {
    ui->convView->append(QTime::currentTime().toString()+ " " + msg);
}

void ChatWindow::onSendClicked() {
    emit sendMessage(mAccount->nickname()+": "+ui->convInput->text());
    ui->convInput->clear();
}

void ChatWindow::toggleWhiteboard() {
    if(canvasViewShown) {
        canvasView->setShown(false);
        resize(mSize);
        setMaximumSize(mSize);
        setMaximumHeight(MAXSIZE);
        setMaximumWidth(MAXSIZE);
    }
    else {
        mSize = size();
        canvasView->setShown(true);
        resize(800, 700);
        canvasView->setMinimumSize(800,550);
    }
    canvasViewShown = !canvasViewShown;
}

void ChatWindow::setConnection(const QDBusConnection &dbusconn) {
    MaknetoCanvas *maknetocanvas = canvas->canvas();
    //FreeHandTool *freeHandTool = canvas->freeHandTool();

    if(mDBusConnection.isConnected()) {
        mDBusConnection.disconnect(QString(), "/makneto", "org.qt.standal.makneto", "sendMessage", this, SLOT(addMessage(QString)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "paintNew", maknetocanvas, SLOT(paintElement(int,QPoint,QPoint)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "mPropertyChanged", maknetocanvas, SLOT(changeObjectProperty(int, QString, int)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "textChanged", maknetocanvas, SLOT(changeObjectProperty(int, QString)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "destroyObject", maknetocanvas, SLOT(destroyElement(int)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "lineDrawn", maknetocanvas, SLOT(drawLineOn(int,QPointF,QPointF)));
        mDBusConnection.disconnect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "clearCanvas", maknetocanvas, SLOT(clearCanvasOf(int)));
        mDBusConnection.disconnectFromPeer("makneto");
    }
    qDebug() << "ChatWindow is now connected via" << mAccount->uniqueIdentifier();
    mDBusConnection = dbusconn;

    if (!mDBusConnection.registerObject("/makneto", this, QDBusConnection::ExportAllSignals | QDBusConnection::ExportAllSlots)) {
        qDebug() << "Failed to register Makneto MainWindow to DBus";
    }
    if (!mDBusConnection.registerObject("/makneto/canvas", maknetocanvas, QDBusConnection::ExportAllSignals | QDBusConnection::ExportAllSlots)) {
        qDebug() << "Failed to register MaknetoCanvas to DBus";
    }

    if (!mDBusConnection.connect(QString(), "/makneto", "org.qt.standal.makneto", "sendMessage", this, SLOT(addMessage(QString)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.sendMessage()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "paintNew", maknetocanvas, SLOT(paintElement(int,QPoint,QPoint, int)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.paintNew()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "mPropertyChanged", maknetocanvas, SLOT(changeObjectProperty(int, QString, int)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.mPropertyChanged()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "textChanged", maknetocanvas, SLOT(changeObjectProperty(int, QString)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.textChanged()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "destroyObject", maknetocanvas, SLOT(destroyElement(int)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.destroyObject()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "lineDrawn", maknetocanvas, SLOT(drawLineOn(int,QPointF,QPointF)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.lineDrawn()";
    }
    if (!mDBusConnection.connect(QString(), "/makneto/canvas", "org.qt.standal.makneto", "clearCanvas", maknetocanvas, SLOT(clearCanvasOf(int)))) {
        qDebug() << "Failed to connect to org.qt.standal.makneto.clearCanvas()";
    }
}

void ChatWindow::testingOutput() {
    qDebug() << "got the signal in chatwindow";
}
