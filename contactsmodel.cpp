/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "contactsmodel.h"

#include <TelepathyQt/Contact>
#include <TelepathyQt/ContactManager>
#include <TelepathyQt/Account>
#include <TelepathyQt/AccountManager>
#include <TelepathyQt/Presence>
#include <TelepathyQt/PendingReady>

Q_DECLARE_METATYPE(Tp::ContactPtr)
Q_DECLARE_METATYPE(Tp::AccountPtr)
Q_DECLARE_METATYPE(Tp::AccountManagerPtr)

ContactsModel::ContactsModel(QObject *parent) :
    QStandardItemModel(parent)
{
    QHash<int, QByteArray> roles = roleNames();
    roles[Qt::DisplayRole] = "displayName";
    roles[IconNameRole] = "iconName";
    setRoleNames(roles);
}

ContactsModel::~ContactsModel() {
}

void ContactsModel::accountAdded(const Tp::AccountPtr &acc) {
    if(acc->connection().isNull()) return;
    connect(acc.data(), SIGNAL(connectionChanged(Tp::ConnectionPtr)), SLOT(accountOffline()));

    Tp::ContactManagerPtr manager = acc->connection()->contactManager();
    connect(manager.data(), SIGNAL(allKnownContactsChanged(Tp::Contacts,Tp::Contacts,Tp::Channel::GroupMemberChangeDetails)),
            SLOT(contactsChanged(Tp::Contacts,Tp::Contacts)));

    Tp::Contacts contacts = manager->allKnownContacts();   // ziskat kontakty pro dany account
    contactsChanged(contacts, Tp::Contacts());
}

void ContactsModel::setAccountManager(QObject *accMgr) {
    Tp::AccountManagerPtr mgr(qobject_cast<Tp::AccountManager *>(accMgr));
    mAccountManager = mgr;

    connect(mAccountManager->becomeReady(), SIGNAL(finished(Tp::PendingOperation *)), SLOT(accountManagerReady(Tp::PendingOperation *)));
}

QObject *ContactsModel::accountManager() {
    Tp::AccountManager *mgr = mAccountManager.data();;
    return qobject_cast<QObject *>(mgr);
}

void ContactsModel::accountManagerReady(Tp::PendingOperation *op) {
    Q_UNUSED(op)
    connect(mAccountManager.data(), SIGNAL(newAccount(Tp::AccountPtr)), SLOT(accountAdded(Tp::AccountPtr)));

    QList<Tp::AccountPtr> accounts = mAccountManager->allAccounts();
    foreach(const Tp::AccountPtr &account, accounts) {
        accountAdded(account);
    }

}

int ContactsModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);   // nevyhazuj warning
    return 1;
}

int ContactsModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return mContacts.count();
}

QModelIndex ContactsModel::parent(const QModelIndex &child) const {
    Q_UNUSED(child);
    return QModelIndex();
}

QModelIndex ContactsModel::index(int row, int column, const QModelIndex &parent) const {
    // vytvori index pro praci s prvkem na row,column
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QVariant ContactsModel::data(const QModelIndex &index, int role) const {
    if(!index.isValid()) return QVariant();

    int row = index.row();
    if(row < 0 || row >= mContacts.count()) return QVariant();

    const Tp::ContactPtr contact = mContacts.at(row);

    switch(role) {
        case Qt::DisplayRole:
            return contact->alias();
        case Qt::DecorationRole:
            return QIcon::fromTheme(data(index, IconNameRole).toString());
        case IconNameRole: {
            switch(contact->presence().type()) {
                case Tp::ConnectionPresenceTypeUnset:
                case Tp::ConnectionPresenceTypeOffline:
                case Tp::ConnectionPresenceTypeUnknown:
                case Tp::ConnectionPresenceTypeError:
                case Tp::ConnectionPresenceTypeHidden:
                    return QLatin1String("user-offline");
                case Tp::ConnectionPresenceTypeAvailable:
                    return QLatin1String("user-online");
                case Tp::ConnectionPresenceTypeAway:
                case Tp::ConnectionPresenceTypeExtendedAway:
                    return QLatin1String("user-away");
                case Tp::ConnectionPresenceTypeBusy:
                    return QLatin1String("user-busy");
                default: return QString();
            }
        }
        case ContactRole:
            return QVariant::fromValue(contact);
        case AccountRole:
            return QVariant::fromValue(accountFromContact(contact));
        case StatusRole: {
            switch(contact->presence().type()) {
                case Tp::ConnectionPresenceTypeUnset:
                case Tp::ConnectionPresenceTypeOffline:
                case Tp::ConnectionPresenceTypeUnknown:
                case Tp::ConnectionPresenceTypeError:
                case Tp::ConnectionPresenceTypeHidden:
                    return Offline;
                case Tp::ConnectionPresenceTypeAvailable:
                    return Available;
                case Tp::ConnectionPresenceTypeAway:
                case Tp::ConnectionPresenceTypeExtendedAway:
                    return Away;
                case Tp::ConnectionPresenceTypeBusy:
                    return Busy;
                default: return Error;
            }
        }
    }
    return QVariant();
}

Tp::AccountPtr ContactsModel::accountFromContact(const Tp::ContactPtr &contact) const {
    foreach(const Tp::AccountPtr &acc, mAccountManager->allAccounts()) {
        if(acc->connection() == contact->manager()->connection()) return acc;
    }
    return Tp::AccountPtr();
}

void ContactsModel::accountOffline() {
    // removed or went offline
}

void ContactsModel::contactsChanged(const Tp::Contacts &added, const Tp::Contacts &removed) {
    foreach(const Tp::ContactPtr &contact, added) {
        connect(contact.data(), SIGNAL(presenceChanged(Tp::Presence)), SLOT(contactChanged()));
    }
    if(added.count() > 0) {
        beginInsertRows(QModelIndex(), mContacts.count(), mContacts.count() + added.count()-1);
            mContacts.append(added.toList());
        endInsertRows();
    }
    foreach(const Tp::ContactPtr &contact, removed) {
        int row = mContacts.indexOf(contact);   // returns -1 if not exists
        if(row >= 0) {
            beginRemoveRows(QModelIndex(), row, row);
                mContacts.removeAll(contact);
            endRemoveRows();
        }
    }
    emit toProxySort();
}

void ContactsModel::contactChanged() {
    Tp::ContactPtr contact(qobject_cast<Tp::Contact *>(sender()));   // sender returns caller
    int row = mContacts.indexOf(contact);
    if(row < 0) return;
    QModelIndex id = index(row, 1, QModelIndex());
    emit dataChanged(id, id);   //
    emit toProxySort();
}
