# Add more folders to ship with the application, here
folder_01.source = qml/contactlist
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QT += core gui dbus

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

TARGET = makneto
TEMPLATE = app

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp contactsmodel.cpp \
    contactsview.cpp \
    iconprovider.cpp \
    chatwindow.cpp \
    tubehandler.cpp \
    msortfilterproxymodel.cpp \
    maknetocanvas.cpp \
    freehandtool.cpp \
    canvaswindow.cpp

HEADERS += contactsmodel.h \
    contactsview.h \
    iconprovider.h \
    chatwindow.h \
    tubehandler.h \
    msortfilterproxymodel.h \
    maknetocanvas.h \
    freehandtool.h \
    canvaswindow.h \
    line.h

INCLUDEPATH += /usr/include/telepathy-qt4

LIBS += -ltelepathy-qt4

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

FORMS += \
    chatwindow.ui

service.path = /usr/share/dbus-1/services/
service.files = org.freedesktop.Telepathy.Client.Makneto.service
client.path = /usr/share/telepathy/clients/
client.files = Makneto.client
makneto.path = /usr/bin
makneto.files = makneto
qmlcontacts.path = /usr/bin/qml/contactlist
qmlcontacts.files = qml/contactlist/SearchField.qml \
                    qml/contactlist/main.qml
qmlcanvas.path = /usr/bin/qml/canvas
qmlcanvas.files =  qml/canvas/MkText.qml \
    qml/canvas/MkRectangle.qml \
    qml/canvas/FreeHandCanvas.qml \
    qml/canvas/Canvas.qml \
    qml/canvas/MkLine.qml

INSTALLS += makneto qmlcontacts qmlcanvas service client

OTHER_FILES += \
    qml/contactlist/SearchField.qml \
    qml/canvas/MkText.qml \
    qml/canvas/MkRectangle.qml \
    qml/canvas/FreeHandCanvas.qml \
    qml/canvas/Canvas.qml \
    qml/canvas/MkLine.qml
