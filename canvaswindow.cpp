/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "canvaswindow.h"
#include "maknetocanvas.h"
#include "freehandtool.h"
#include "line.h"

#include <QToolBar>
#include <QActionGroup>
#include <QListWidget>

CanvasWindow::CanvasWindow(QWidget *parent) :
    QMainWindow(parent)
{
    qmlRegisterType<FreeHandTool>("Makneto", 2, 0, "FreeHand");
    qmlRegisterType<Line>("Makneto", 2, 0, "Line");
    mCanvas = new MaknetoCanvas();
    resize(800, 500);   // the canvas needs some size
    mCanvasPaintRoot = mCanvas->paintingRoot();

    addElementsToolBar();

    setCentralWidget(mCanvas);
}


void CanvasWindow::testing() {
    qDebug() << "got the signal in CanvasWindow";
}

CanvasWindow::~CanvasWindow()
{
}


void CanvasWindow::addListWidget() {
    mComponentsList = new QListWidget(this);
    addListWidget();


}

void CanvasWindow::addElementsToolBar() {
    QToolBar *toolbar = new QToolBar(tr("Elements"), this);
    toolbar->setOrientation(Qt::Vertical);
    addToolBar(Qt::LeftToolBarArea, toolbar);

    QActionGroup *actionGroup = new QActionGroup(this);
    QAction *act = new QAction(this);
    act->setText("Move&&&draw");
    act->setCheckable(true);
    act->setChecked(true);
    connect(act, SIGNAL(triggered()), mCanvas, SLOT(selectMove()));
    actionGroup->addAction(act);

    act = new QAction(this);
    act->setText("Line");
    act->setCheckable(true);
    connect(act, SIGNAL(triggered()), mCanvas, SLOT(selectLine()));
    actionGroup->addAction(act);

    act = new QAction(this);
    act->setText("Rectagle");
    act->setCheckable(true);
    connect(act, SIGNAL(triggered()), mCanvas, SLOT(selectRect()));
    actionGroup->addAction(act);

    act = new QAction(this);
    act->setText("Text");
    act->setCheckable(true);
    connect(act, SIGNAL(triggered()), mCanvas, SLOT(selectText()));
    actionGroup->addAction(act);

    act = new QAction(this);
    act->setText("FreeHand");
    act->setCheckable(true);
    connect(act, SIGNAL(triggered()), mCanvas, SLOT(selectFreeHand()));
    actionGroup->addAction(act);

    toolbar->addActions(actionGroup->actions());
}

QObject *CanvasWindow::canvasPaintRoot() const {
    return mCanvasPaintRoot;
}

FreeHandTool *CanvasWindow::freeHandTool() const {
    return mCanvas->freeHandTool();
}

MaknetoCanvas *CanvasWindow::canvas() const {
    return mCanvas;
}
