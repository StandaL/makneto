/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "maknetocanvas.h"
#include "freehandtool.h"

#include <QDir>
#include <QCoreApplication>
#include <QDeclarativeEngine>
#include <QDeclarativeItem>
#include <QMouseEvent>
#include <QMutex>
#include <QHash>
#include <QDebug>

#include <limits>

MaknetoCanvas::MaknetoCanvas()
{
    setResizeMode(SizeRootObjectToView);
    QString filePath = resourcePath() + "/Canvas.qml";
    QDeclarativeComponent *mRootComponent = new QDeclarativeComponent(engine(), filePath, this);
    mRootObject = mRootComponent->create(engine()->rootContext());
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(mRootObject);

    mSelected = MOVE;   // as the default checked action in canvaswindow is MOVE

    item->setWidth(width());
    item->setHeight(height());
    setRootObject(item);

    connect(this, SIGNAL(paintNew(int, QPoint, QPoint, int)), SLOT(paintElement(int, QPoint, QPoint, int)));
    connect(this, SIGNAL(destroyObject(int)), SLOT(destroyElement(int)));

    mStartPoint = 0;
    canvasObjects = new QHash<int, QDeclarativeItem *>();
    objectsCounter = 0;
}

void MaknetoCanvas::selectMove() {
    mStartPoint = 0;
    mSelected = MOVE;
}

void MaknetoCanvas::selectLine() {
    mStartPoint = 0;
    mSelected = LINE;
}

void MaknetoCanvas::selectRect() {
    mStartPoint = 0;
    mSelected = RECT;
}

void MaknetoCanvas::selectText() {
    mStartPoint = 0;
    mSelected = TEXT;
}

void MaknetoCanvas::selectFreeHand() {
    mStartPoint = 0;
    mSelected = FREEHAND;
}


QString MaknetoCanvas::resourcePath() {
    return QDir::cleanPath(QCoreApplication::applicationDirPath() + QLatin1String("/qml/canvas/"));
}

int MaknetoCanvas::maxOwnObjectName() const {
    return (std::numeric_limits<int>::max()/2);
}

int MaknetoCanvas::addToHash() {
    static QMutex counterLock;
    QDeclarativeItem *item;

    counterLock.lock();
    while(canvasObjects->find(objectsCounter) != canvasObjects->end()) {
        if(objectsCounter == maxOwnObjectName())
            objectsCounter = 0;
        else objectsCounter++;
    }
    int insertPos = objectsCounter;
    canvasObjects->insert(insertPos, item);
    counterLock.unlock();
   return insertPos;

}

void MaknetoCanvas::mouseReleaseEvent(QMouseEvent *event) {
    if(event->button() == Qt::LeftButton && mSelected != MOVE) {
        if(mStartPoint == 0) mStartPoint = new QPoint(event->pos());
        else {
            emit paintNew(mSelected, *mStartPoint, event->pos(), addToHash());
            mStartPoint = 0;
        }
    }
    QDeclarativeView::mouseReleaseEvent(event);
}

void MaknetoCanvas::paintElement(int shape, const QPoint &point, const QPoint &point2, const int name) {
    QString filePath;
    switch(shape) {
        case RECT:
            filePath = resourcePath() + QLatin1String("/MkRectangle.qml");
            break;
        case TEXT:
            filePath = resourcePath() + QLatin1String("/MkText.qml");
            break;
        case FREEHAND:
            filePath = resourcePath() + QLatin1String("/FreeHandCanvas.qml");
            break;
        case LINE:
            filePath = resourcePath() + QLatin1String("/MkLine.qml");
            break;
        default:
            return;
    }
    QDeclarativeComponent *comp = new QDeclarativeComponent(engine(), filePath, this);
    QPointer<QObject> elem = comp->create(engine()->rootContext());
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(elem);

    QDeclarativeItem *root = paintingRoot();

    if(shape == LINE) {
        item->setParentItem(root);

        connect(item, SIGNAL(destroyMe()), this, SLOT(notifyDestroy()));

        item->setProperty("m_x1", point.x());
        item->setProperty("m_y1", point.y());
        item->setProperty("m_x2", point2.x());
        item->setProperty("m_y2", point2.y());

        connect(item, SIGNAL(x1Changed()), this, SLOT(notifyX1Change()));
        connect(item, SIGNAL(y1Changed()), this, SLOT(notifyY1Change()));
        connect(item, SIGNAL(x2Changed()), this, SLOT(notifyX2Change()));
        connect(item, SIGNAL(y2Changed()), this, SLOT(notifyY2Change()));

        connect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
        connect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
        item->setProperty("objectName", QString::number(name));
        canvasObjects->insert(name, item);
        return;
    }
    int x = (point.x() < point2.x())? point.x() : point2.x();
    int y = (point.y() < point2.y())? point.y() : point2.y();

    item->setPos(x,y);
    item->setParentItem(root);

    connect(item, SIGNAL(destroyMe()), this, SLOT(notifyDestroy()));

    int width = abs(point2.x() - point.x());
    int height = abs(point2.y() - point.y());

    item->setProperty("width", width);
    item->setProperty("height", height);
    item->setProperty("objectName", QString::number(name));
    canvasObjects->insert(name, item);
    connect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
    connect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
    connect(item, SIGNAL(mWidthChanged()), this, SLOT(notifyWidthChange()));
    connect(item, SIGNAL(mHeightChanged()), this, SLOT(notifyHeightChange()));

    if(shape == TEXT) {
        //item->setProperty("activeFocus", "true");
        connect(item, SIGNAL(mTextChanged()), this, SLOT(notifyTextChange()));
    }
    if(shape == FREEHAND) {
        FreeHandTool *freehand = item->findChild<FreeHandTool *>(QString::number(name));
        connect(freehand, SIGNAL(paintLine(QPointF, QPointF)), this, SLOT(notifyDrawLine(QPointF,QPointF)));
        connect(freehand, SIGNAL(clearCanvas()), this, SLOT(notifyClearCanvas()));
    }

}

void MaknetoCanvas::mousePressEvent(QMouseEvent *event) {
    if((mSelected == MOVE)  && event->button() == Qt::LeftButton) {
        // this should assure elements are movable only when "MOVE" is selected and l
        // the objects still react on other mouse button events
        QDeclarativeView::mousePressEvent(event);
    }
    else if(event->button() != Qt::LeftButton) {
        QDeclarativeView::mousePressEvent(event);
    }
}


QDeclarativeItem *MaknetoCanvas::paintingRoot() const {
    return rootObject()->findChild<QDeclarativeItem *>("paintRoot");
}

FreeHandTool *MaknetoCanvas::freeHandTool() const {
    return rootObject()->findChild<FreeHandTool *>("freeHandTool");
}

void MaknetoCanvas::notifyXChange() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "x", item->property("x").value<int>());
}

void MaknetoCanvas::notifyYChange() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "y", item->property("y").value<int>());
}

void MaknetoCanvas::notifyX1Change() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "m_x1", item->property("m_x1").value<int>());
}

void MaknetoCanvas::notifyX2Change() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "m_x2", item->property("m_x2").value<int>());
}

void MaknetoCanvas::notifyY1Change() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "m_y1", item->property("m_y1").value<int>());
}

void MaknetoCanvas::notifyY2Change() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "m_y2", item->property("m_y2").value<int>());
}

void MaknetoCanvas::notifyWidthChange() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "mWidth", item->property("mWidth").value<int>());
}

void MaknetoCanvas::notifyHeightChange() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit mPropertyChanged(nameNum, "mHeight", item->property("mHeight").value<int>());
}

void MaknetoCanvas::notifyTextChange() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit textChanged(nameNum, item->property("mText").value<QString>());
}

void MaknetoCanvas::notifyDestroy() {
    QDeclarativeItem *item = qobject_cast<QDeclarativeItem *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit destroyObject(nameNum);
}

void MaknetoCanvas::notifyDrawLine(const QPointF &from, const QPointF &to) {
    FreeHandTool *item = qobject_cast<FreeHandTool *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit lineDrawn(nameNum, from, to);
}

void MaknetoCanvas::notifyClearCanvas() {
    FreeHandTool *item = qobject_cast<FreeHandTool *>(sender());
    QString name = item->property("objectName").value<QString>();
    int nameNum = name.toInt();
    emit clearCanvas(nameNum);
}

void MaknetoCanvas::changeObjectProperty(const int name, const QString &property, const int value) {
    QHash<int, QDeclarativeItem *>::iterator it = canvasObjects->find(name);
    if(it == canvasObjects->end()) return;
    QDeclarativeItem *item = it.value();
    // we have to disconnect all signals so that the signal of property change doesnt go back and forth in a loop
    if((item->findChild<QDeclarativeItem *>("handle1") != 0)) {
        if(property == "x") {
            disconnect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
            item->setProperty("x", value);
            connect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
        }
        if(property == "y") {
            disconnect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
            item->setProperty("y", value);
            connect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
        }
        disconnect(item, SIGNAL(x1Changed()), this, SLOT(notifyX1Change()));
        disconnect(item, SIGNAL(y1Changed()), this, SLOT(notifyY1Change()));
        disconnect(item, SIGNAL(x2Changed()), this, SLOT(notifyX2Change()));
        disconnect(item, SIGNAL(y2Changed()), this, SLOT(notifyY2Change()));
        QDeclarativeItem *handle1 = item->findChild<QDeclarativeItem *>("handle1");
        QDeclarativeItem *handle2 = item->findChild<QDeclarativeItem *>("handle2");
        if(property == "m_x1") handle1->setProperty("x", value);
        if(property == "m_y1") handle1->setProperty("y", value);
        if(property == "m_x2") handle2->setProperty("x", value);
        if(property == "m_y2") handle2->setProperty("y", value);
        connect(item, SIGNAL(x1Changed()), this, SLOT(notifyX1Change()));
        connect(item, SIGNAL(y1Changed()), this, SLOT(notifyY1Change()));
        connect(item, SIGNAL(x2Changed()), this, SLOT(notifyX2Change()));
        connect(item, SIGNAL(y2Changed()), this, SLOT(notifyY2Change()));
        return;
    }
    disconnect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
    disconnect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
    disconnect(item, SIGNAL(mWidthChanged()), this, SLOT(notifyWidthChange()));
    disconnect(item, SIGNAL(mHeightChanged()), this, SLOT(notifyHeightChange()));
    if((property == "x") || (property == "y"))
        item->setProperty((const char *)property.data(), value);
    else {
        QDeclarativeItem *handle = item->findChild<QDeclarativeItem *>("handle");
        if(property == "mWidth")
            handle->setProperty("x", value);
        if(property == "mHeight")
            handle->setProperty("y", value);
    }
    connect(item, SIGNAL(xChanged()), this, SLOT(notifyXChange()));
    connect(item, SIGNAL(yChanged()), this, SLOT(notifyYChange()));
    connect(item, SIGNAL(mWidthChanged()), this, SLOT(notifyWidthChange()));
    connect(item, SIGNAL(mHeightChanged()), this, SLOT(notifyHeightChange()));
}

void MaknetoCanvas::changeObjectProperty(const int name, const QString &value) {
    QHash<int, QDeclarativeItem *>::iterator it = canvasObjects->find(name);
    if(it == canvasObjects->end()) return;
    QDeclarativeItem *item = it.value();
    disconnect(item, SIGNAL(mTextChanged()), this, SLOT(notifyTextChange()));
    QDeclarativeItem *innerItem = item->findChild<QDeclarativeItem *>("innerElement");
    innerItem->setProperty("text", value);
    connect(item, SIGNAL(mTextChanged()), this, SLOT(notifyTextChange()));
}

void MaknetoCanvas::drawLineOn(const int name, const QPointF &from, const QPointF &to) {
    QHash<int, QDeclarativeItem *>::iterator it = canvasObjects->find(name);
    if(it == canvasObjects->end()) return;
    QDeclarativeItem *item = it.value();
    FreeHandTool *freehand = item->findChild<FreeHandTool *>(QString::number(name));
    disconnect(freehand, SIGNAL(paintLine(QPointF,QPointF)), this, SLOT(notifyDrawLine(QPointF,QPointF)));
    freehand->drawLineTo(from, to);
    connect(freehand, SIGNAL(paintLine(QPointF,QPointF)), this, SLOT(notifyDrawLine(QPointF,QPointF)));
}

void MaknetoCanvas::clearCanvasOf(const int name) {
    QHash<int, QDeclarativeItem *>::iterator it = canvasObjects->find(name);
    if(it == canvasObjects->end()) return;
    QDeclarativeItem *item = it.value();
    FreeHandTool *freehand = item->findChild<FreeHandTool *>(QString::number(name));
    disconnect(freehand, SIGNAL(clearCanvas()), this, SLOT(notifyClearCanvas()));
    freehand->clear();
    connect(freehand, SIGNAL(clearCanvas()), this, SLOT(notifyClearCanvas()));
}

void MaknetoCanvas::destroyElement(const int name) {
    QHash<int, QDeclarativeItem *>::iterator it = canvasObjects->find(name);
    if(it == canvasObjects->end()) return;
    QDeclarativeItem *item = it.value();
    canvasObjects->erase(it);
    item->deleteLater();
}
