/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "msortfilterproxymodel.h"
#include <QDebug>

MSortFilterProxyModel::MSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

void MSortFilterProxyModel::mSetSourceModel(QObject *baseModel) {
    setSourceModel(qobject_cast<QAbstractItemModel *>(baseModel));
}

void MSortFilterProxyModel::sortOnChanged() {
    sort(0);
    setFilterRegExp("      ");
    setFilterRegExp("");    // this, actually, is a hack to force the QML ListView to update
    // saving old RegExp and then forcing it back is not recommended, ListView behaves in a strange way
    // curiously, only one space breaks contact list update on start
}

QString MSortFilterProxyModel::mFilterRegExp() {
    return filterRegExp().pattern();
}

bool MSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const {
    QVariant leftData = sourceModel()->data(left, sortRole());
    QVariant rightData = sourceModel()->data(right, sortRole());
    if(leftData.toInt() == rightData.toInt()) {
        leftData = sourceModel()->data(left, Qt::DisplayRole);
        rightData = sourceModel()->data(right, Qt::DisplayRole);
        return leftData.toString().toLower().localeAwareCompare(rightData.toString().toLower()) < 0;
    }
    else return leftData.toInt() < rightData.toInt();
}
