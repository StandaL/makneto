/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef FREEHANDTOOL_H
#define FREEHANDTOOL_H

#include <QDeclarativeItem>

class FreeHandTool : public QDeclarativeItem
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.qt.standal.makneto")
    Q_PROPERTY(int penWidth READ penWidth WRITE setPenWidth)
    Q_PROPERTY(QColor penColor READ penColor WRITE setPenColor)
    //Q_PROPERTY(bool drawingAllowedDis READ drawingAllowed NOTIFY drawingAllowedChanged)

public:
    explicit FreeHandTool(QDeclarativeItem *parent = 0);
    void setPenWidth(const int);
    int penWidth() const;
    void setPenColor(const QColor &);
    QColor penColor() const;

    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    //virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    virtual void paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *);
    
signals:
    void paintLine(QPointF, QPointF);
    void clearCanvas();
    //void drawingAllowedChanged();
    
public slots:
    void drawLineTo(const QPointF &, const QPointF &);
    void drawLineTo(const qreal, const qreal, const qreal, const qreal);
    void clear();
    bool isEmpty() const;

    void foreignDrawLineTo(const QPointF &, const QPointF &);
    void foreignClear();

private slots:
    //void slotFocusChanged();

private:

    bool mDrawing;
    bool mDrawingAllowed;
    bool modified;
    int mPenWidth;
    QColor mPenColor;
    QPointF fromPoint;
    QPointF toPoint;
    QPixmap *mPixmap;
    QVector<QPointF> *mPoints;
    QVector<QPointF> *foreignPoints;
};

#endif // FREEHANDTOOL_H
