/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 1.1
import Makneto 2.0

Item {
    property alias mWidth: handle.x
    property alias mHeight: handle.y
    signal mHeightChanged();
    signal mWidthChanged();

    id: root
    signal destroyMe()

    Rectangle {
        id: freehandRect

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: handle.horizontalCenter
        anchors.bottom: handle.verticalCenter

        border.width: 1
        border.color: (freehand.focus || freehand.isEmpty()) ? "black" :"transparent"
        color: "transparent"
        onWidthChanged: root.mWidthChanged()
        onHeightChanged: root.mHeightChanged()

        FreeHand {
            id: freehand
            objectName: root.objectName // in C++ I connect signals from this element with some slots; this = easiest way
            anchors.fill: parent
            anchors.margins: 1  // the element sometimes caused some of the parent Rectangle borders not to drop on focus lost
            smooth: true
            focus: true
        }
    }

    MouseArea {
        id: mousearea
        x:0
        y:0
        width: freehandRect.width
        height: freehandRect.height
        acceptedButtons: Qt.RightButton
        onClicked: {
            if(mouse.button === Qt.RightButton)
                freehand.focus ? freehand.clearCanvas() : root.destroyMe()
        }
    }
    Rectangle {
        property bool resize: true

        id: handle
        objectName: "handle"
        width: 16
        height: 16
        radius: width/2
        smooth: true
        color: resize? Qt.darker("gray") : Qt.lighter("red")
        border.width: 2
        border.color: resize? Qt.lighter("red") : "gray"
        x: root.width
        y: root.height
        z: freehandRect.z+1
        visible: freehand.focus
        MouseArea {
            id: handleMouseArea
            anchors.fill: handle
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onDoubleClicked: handle.resize = !handle.resize
            drag {
                target: handle.resize ? handle : root
                minimumX: handle.resize ? 5 : -root.width // we dont want the handle to overflow root border
                minimumY: handle.resize ? 5 : -root.height
            }
        }
    }
}
