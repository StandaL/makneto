/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 1.1
import Makneto 2.0

Item {
    id: root
    Rectangle {
        id: canvasRect
        anchors.fill: parent
        anchors.rightMargin: coloursPalette.width
        border.width: 1
        border.color: "#AAA"

        MouseArea {
            id: focusHack
            anchors.fill: parent
            onClicked: {
                focusGround.forceActiveFocus()
                focusGround.focus = true
            }
        }

        FocusScope {
            id: focusScope
            objectName: "paintRoot"
            anchors.fill: parent
            /*FreeHand {
                objectName: "freeHandTool"
                anchors.fill: parent
                smooth: true
            }*/
            TextEdit {
                id: focusGround
                x: -10
                y: -10
                readOnly: true
            }
        }
    }
    Rectangle {
        id: coloursPalette
        anchors.left: canvasRect.right
        anchors.top: root.top
        width: 0    // colours temporarily not implemented
        height: root.height
        color: "gray"
        visible: false
    }
}
