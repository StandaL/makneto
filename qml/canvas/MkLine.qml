/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 1.1
import Makneto 2.0

Item {
    property alias m_x1: handle1.x
    property alias m_y1: handle1.y
    property alias m_x2: handle2.x
    property alias m_y2: handle2.y

    signal x1Changed()
    signal y1Changed()
    signal x2Changed()
    signal y2Changed()

    signal destroyMe()

    id: lineRoot

    Line {
        id: line

        x1: handle1.x + handle1.width/2; y1: handle1.y + handle1.width/2
        x2: handle2.x + handle2.width/2; y2: handle2.y + handle2.width/2
        color: "tomato"; penWidth: 3; smooth: true
    }
    Rectangle {
        id: handle1
        objectName: "handle1"
        width: 10
        height: 10
        radius: 10
        color: Qt.darker("black")
        z: line.z+1
        onXChanged: lineRoot.x1Changed()
        onYChanged: lineRoot.y1Changed()
        MouseArea {
            id: handle1MouseArea
            anchors.fill: handle1
            drag.target: handle1
            onClicked: line.focus = true
            onPressed: line.focus = true
        }
    }
    Rectangle {
        id: handle2
        objectName: "handle2"
        width: 10
        height: 10
        radius: 10
        color: Qt.darker("black")
        z: line.z+1
        onXChanged: lineRoot.x2Changed()
        onYChanged: lineRoot.y2Changed()
        MouseArea {
            id: handleMouseArea
            anchors.fill: handle2
            drag.target: handle2
            onClicked: line.focus = true
            onPressed: line.focus = true
        }
    }
    MouseArea {
        anchors.fill: line
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if(mouse.button === Qt.RightButton) lineRoot.destroyMe()
            if(mouse.button === Qt.LeftButton) line.focus = true
        }
        onPressed: line.focus = true
        drag.target: lineRoot
    }
}
