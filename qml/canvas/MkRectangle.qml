/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 1.1

Item {
    property alias mWidth: handle.x
    property alias mHeight: handle.y
    signal mHeightChanged();
    signal mWidthChanged();

    signal destroyMe()
    id: root

    focus: true
    Rectangle {
        id: rect
        objectName: "innerElement"

        color: Qt.lighter("blue")
        border.width: 1
        border.color: "black"
        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: handle.horizontalCenter;
        anchors.bottom: handle.verticalCenter;

        onWidthChanged: root.mWidthChanged()
        onHeightChanged: root.mHeightChanged()

    }
    MouseArea {
        x:0
        y:0
        width: rect.width
        height: rect.height
        drag.target: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if(mouse.button === Qt.RightButton) root.destroyMe()
            if(mouse.button === Qt.LeftButton) root.focus = true
        }
        onPressAndHold: if(mouse.button === Qt.LeftButton) root.focus = true
    }
    Rectangle {
        id: handle
        objectName: "handle"
        visible: root.focus
        width: 8
        height: 8
        color: Qt.darker(rect.color)
        x: root.width
        y: root.height
        z: rect.z+1
        MouseArea {
            id: handleMouseArea
            anchors.fill: handle
            drag {
                target: handle
                minimumX: 5 // we dont want the handle to overflow root border
                minimumY: 5
            }
        }
    }
}
