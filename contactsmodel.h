/*
 * Copyright (C) 2013 Stanislav Laznicka <standal@windowslive.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef CONTACTSMODEL_H
#define CONTACTSMODEL_H

#include <QStandardItemModel>
#include <TelepathyQt/Types>
#include <TelepathyQt/PendingReady>

class ContactsModel : public QStandardItemModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *accountManager READ accountManager WRITE setAccountManager)

public:
    enum {
        AccountRole = Qt::UserRole+1,   // UserRole je posledni definovana role, +1 tedy prvni nase
        ContactRole,
        IconNameRole,
        StatusRole
    };

    enum {
        Available,
        Invisible,
        Busy,
        Away,
        Unavailable,
        Offline,
        Error
    };

    explicit ContactsModel(QObject *parent = 0);
    virtual ~ContactsModel();

    void setAccountManager(QObject *accMgr);
    QObject *accountManager();
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int columnCount(const QModelIndex &parent) const;

signals:
    void toProxySort();

public slots:
    void accountAdded(const Tp::AccountPtr &acc);
    void accountOffline();
    void contactChanged();
    void contactsChanged(const Tp::Contacts &added, const Tp::Contacts &removed);

private slots:
    void accountManagerReady(Tp::PendingOperation *op);

private:
    Tp::AccountPtr accountFromContact(const Tp::ContactPtr &) const;
    Tp::AccountManagerPtr mAccountManager;
    QList<Tp::ContactPtr> mContacts;
};

#endif // CONTACTSMODEL_H
